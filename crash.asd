(asdf:defsystem #:crash
  :description "Yet Another Shell"
  :author "Florian Margaine <florian@margaine.com>"
  :license "MIT License"
  :serial t
  :depends-on (:cl-readline
               :sb-posix
               :cffi
               :cl-ppcre
               :com.informatimago.common-lisp.lisp-reader)
  :build-operation asdf:program-op
  :build-pathname "bin/crash"
  :entry-point "crash:main"
  :components ((:module "src"
                        :components ((:file "package")
                                     (:file "crash" :depends-on ("prompt" "defaults" "module"))
                                     (:file "prompt" :depends-on ("package"))
                                     (:file "hooks" :depends-on ("package"))
                                     (:file "defaults" :depends-on ("hooks"))
                                     (:file "module" :depends-on ("package"))))))
