(in-package #:crash)

;; Shamelessly stolen from stumpwm.
(defun build-load-path (path)
  "Maps subdirectories of path, returning a list of all subdirs in the
  path which contain any files ending in .asd"
  (mapcar #'pathname
          (mapcar #'directory-namestring (directory (uiop:strcat
                                                     (namestring path)
                                                     "**/*.asd")))))

(defun setup-modules-load-path (path)
  (setf asdf:*central-registry*
        (union (build-load-path path) asdf:*central-registry*)))

(export 'load-module)
(defun load-module (module)
  ;; Hide asdf output.
  (ql:quickload (format nil "crash.~A" module) :silent t))
