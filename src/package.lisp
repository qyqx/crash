(defpackage #:crash
  (:use #:cl)
  (:local-nicknames (#:reader #:com.informatimago.common-lisp.lisp-reader.reader)))

(defpackage #:crash-user
  (:use #:cl #:crash))
