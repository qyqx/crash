(defpackage #:crash.history
  (:use #:cl #:crash))

(in-package #:crash.history)

(asdf:defsystem #:crash.history
  :description "History plugin for crash"
  :author "Florian Margaine <florian@margaine.com>"
  :license "MIT License"
  :serial t
  :depends-on (:crash)
  :components ((:file "history")))
