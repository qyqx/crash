(in-package #:crash.pipe)

(defvar *stdin-fileno* 0)
(defvar *stdout-fileno* 1)

(defhook pipe (:weight -500) (command arguments next)
  (let ((position (position "|" (cons command arguments) :test #'string=)))
    (unless position
      (return-from pipe (funcall next command arguments)))
    (pipe-commands
     (if (= position 0)
         (get-prefix-commands arguments)
         (get-infix-commands (format nil "~A ~{~A~^ ~}" command arguments))))))

(defun pipe-commands (commands)
  (let ((pids nil)
        (old-fds nil)
        (new-fds nil))
    (dotimes (i (length commands))
      (when (nth (1+ i) commands)
        (multiple-value-bind (read-end write-end)
            (sb-posix:pipe)
          (setf (getf new-fds :read-end) read-end
                (getf new-fds :write-end) write-end)))
      (let ((pid (sb-posix:fork)))
        (when (< pid 0)
          (error "Failed to fork."))

        ;; Child
        (when (= pid 0)
          (handler-case
              (progn
                ;; There is a previous command
                (when (and (> i 0) (nth (1- i) commands))
                  (sb-posix:close (getf old-fds :write-end))
                  (sb-posix:dup2 (getf old-fds :read-end) *stdin-fileno*)
                  (sb-posix:close (getf old-fds :read-end)))
                ;; There is a next command
                (when (nth (1+ i) commands)
                  (sb-posix:close (getf new-fds :read-end))
                  (sb-posix:dup2 (getf new-fds :write-end) *stdout-fileno*)
                  (sb-posix:close (getf new-fds :write-end)))
                (crash:run-exec (nth i commands)))
            (error () (uiop:quit -1))))

        ;; Parent
        (push pid pids)
        (when (and (> i 0) (nth (1- i) commands))
          (sb-posix:close (getf old-fds :read-end))
          (sb-posix:close (getf old-fds :write-end)))
        (when (nth (1+ i) commands)
          (setf (getf old-fds :read-end) (getf new-fds :read-end))
          (setf (getf old-fds :write-end) (getf new-fds :write-end)))))
    (dolist (pid pids)
      (sb-posix:waitpid pid 0))))

(defun get-infix-commands (line)
  (mapcar
   (lambda (str) (string-trim '(#\Space) str))
   (let ((mutable-line line))
     (loop
        for position = (position "|" mutable-line :test #'string=)
        collect (subseq mutable-line 0 (when position (1- position)))
        until (null position)
        do (setf mutable-line (subseq mutable-line (1+ position)))))))

(defun get-prefix-commands (arguments)
  (flatten
   (loop
      for arg in arguments
      collect (if (consp arg)
                  (if (string= (if (symbolp (first arg))
                                   (symbol-name (first arg))
                                   (write-to-string (first arg)))
                               "|")
                      (get-prefix-commands (rest arg))
                      (format nil "~{~A~^ ~}" arg))
                  (if (symbolp arg)
                      (symbol-name arg)
                      arg)))))

(defun flatten (l)
  (cond ((null l) nil)
        ((atom l) (list l))
        (t (loop for a in l appending (flatten a)))))
