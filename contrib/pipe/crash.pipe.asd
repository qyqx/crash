(defpackage #:crash.pipe
  (:use #:cl #:crash))

(in-package #:crash.pipe)

(asdf:defsystem #:crash.pipe
  :description "Pipe plugin for crash"
  :author "Florian Margaine <florian@margaine.com>"
  :license "MIT License"
  :serial t
  :depends-on (:crash)
  :components ((:file "pipe")))
